FROM maven:3.8-eclipse-temurin-17-alpine
RUN mvn org.owasp:dependency-check-maven:update-only \
  -DdataDirectory=/owasp \
  && rm -rf /root/.m2/
