// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "MyTestSwiftProject",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MyTestSwiftProjectLib",
            targets: ["Whatever-No-Idea"]),
    ],
    dependencies: [
        // List of dependencies
        .package(url: "https://github.com/Alamofire/Alamofire.git", .exact("5.8.0")),
        .package(url: "https://github.com/Juanpe/SkeletonView.git", .exact("1.25.1")),
        .package(url: "https://github.com/daltoniam/Starscream.git", .exact("4.0.4")),
        .package(url: "https://github.com/highcharts/highcharts-ios.git", .exact("9.1.0")),
        .package(url: "https://github.com/socketio/socket.io-client-swift.git", .exact("16.0.1")),
        .package(url: "https://github.com/uber/ios-snapshot-test-case.git", .exact("6.2.0")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "YourPackageName",
            dependencies: ["Alamofire", "SkeletonView", "Starscream", "Highcharts", "Socket.IO-Client-Swift", "FBSnapshotTestCase"]),
    ]
)
